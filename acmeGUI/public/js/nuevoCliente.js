let registrar = document.querySelector('#registrar');
let cancelar = document.querySelector('#cancelar');
registrar.addEventListener('click', (e) => {
    e.preventDefault();
    let nombre = document.querySelector('#nombre');
    let apellido = document.querySelector('#apellido');
    let cedula = document.querySelector('#cedula');
    let direccion = document.querySelector('#direccion');
    let medidor = document.querySelector('#medidor');
    
    token = getCookie('tokenAcme');
    fetch(
        'http://localhost:8000/api/1.0/cliente/',
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization' : 'Token ' + token
            },
            body: JSON.stringify({
                'nombre': nombre.value,
                'apellido': apellido.value,
                'cedula': cedula.value,
                'direccion': direccion.value,
                'medidor': medidor.value
            })
        }).then(function(response){
            return response.json()
        }).then(function(json){
            if (json.estatus == 1){
                showAlert(json.estatus_descripcion, '', 'success', listarCliente);
            }else if(json.estatus == 0){
                showAlert('¡Error!', json.estatus_descripcion);
            }else{
                texto = getErrors(json)
                showAlert('¡Error!', texto);
            }
        }).catch(function(error){
            console.log(error);
        });
});

agregarElemento = (valor) => {
    var elemento = document.createElement('div');
    elemento.innerHTML = valor;
    return elemento;
}
