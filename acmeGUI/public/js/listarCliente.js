window.addEventListener('load', () => {
    token = getCookie('tokenAcme');
    fetch(
        'http://localhost:8000/api/1.0/cliente/',
        {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization' : 'Token ' + token
            }
        }).then(function(response){
            return response.json()
        }).then(function(json){
            if (json.estatus == 1){
                if (json.estatus_descripcion.length){
                    var lista = document.querySelector('#lista');
                    json.estatus_descripcion.forEach((item, index) => {
                        var registro = document.createElement('div');
                        registro.classList.add('registro');
                        registro.appendChild(agregarElemento(item.nombre + ' ' + item.apellido));
                        registro.appendChild(agregarElemento(item.cedula));
                        registro.appendChild(agregarElemento(item.direccion));
                        registro.appendChild(agregarElemento(item.medidor));
                        registro.appendChild(agregarElemento('<i class="fas fa-eye" data-toggle="tooltip" title="Ver" data-tipo="ver" data-cedula="'+item.cedula+'"></i><i class="fas fa-edit" data-toggle="tooltip" title="Editar" data-tipo="editar" data-cedula="'+item.cedula+'"></i><i class="fas fa-trash-alt" data-toggle="tooltip" title="Eliminar" data-tipo="eliminar" data-cedula="'+item.cedula+'"></i>'));
                        lista.appendChild(registro);
                    });
                }else{
                    var lista = document.querySelector('#lista');
                    var registro = document.createElement('div');
                    registro.classList.add('registro');
                    registro.classList.add('lista-vacia');
                    var elemento = document.createElement('div');
                    elemento.innerHTML = 'No existen clientes para listar';
                    registro.appendChild(elemento);
                    lista.appendChild(registro);
                }
            }
        }).catch(function(error){
            console.log(error);
        });

});

agregarElemento = (valor) => {
    var elemento = document.createElement('div');
    elemento.innerHTML = valor;
    return elemento;
}

let listaElement = document.querySelector('#lista');
listaElement.addEventListener('click', (e) => {
    let tipo = e.target.dataset.tipo;
    let cedula = e.target.dataset.cedula;
    if (tipo && cedula){
        if (tipo == 'ver'){
            var actionURL = listarClienteURL + '/' + cedula;
        }else if(tipo == 'editar'){
            var actionURL = listarClienteURL + '/editar/' + cedula;
        }else if(tipo == 'eliminar'){
            Swal.fire({
                icon: 'warning',
                title: '¡Esta a punto de eliminar un registro!',
                html: '¿Esta seguro de querer eliminarlo?',
                allowOutsideClick: false,
                allowEscapeKey: false,
                showCancelButton: true,
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar',
                cancelButtonColor: '#d33',
              }).then(function(result){
                if (result.value){
                    let token = getCookie('tokenAcme');
                    let requestURL = 'http://localhost:8000/api/1.0/cliente/' + cedula;
                    fetch(
                        requestURL,
                        {
                            method: 'DELETE',
                            headers: {
                                'Content-Type': 'application/json',
                                'Authorization' : 'Token ' + token
                            }
                        }).then(function(response){
                            return response.json()
                        }).then(function(json){
                            if (json.estatus == 0){
                                showAlert('¡Error!', 'Error al procesar su solicitud');
                                return false;
                            }else{
                                showAlert('¡Registro eliminado!', json.status_descripcion, 'success', listarClienteURL);
                            }
                        }).catch(function(error){
                            console.log(error);
                        });
                    var actionURL = listarClienteURL;
                }else if(result.dismiss === Swal.DismissReason.cancel){
                    showAlert('¡Operación cancelada!', 'Ha cancelado la operación de eliminación');
                }
              });
        }
        relocate(actionURL);
    }
}, false);

