window.addEventListener('load', () => {
    let token = getCookie('tokenAcme');
    let requestURL = 'http://localhost:8000/api/1.0/cliente/' + cedula;
    fetch(
        requestURL,
        {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization' : 'Token ' + token
            }
        }).then(function(response){
            return response.json()
        }).then(function(json){
            if (json.estatus == 1){
                console.log(json);
                document.querySelector('#nombre').value = json.estatus_descripcion.nombre;
                document.querySelector('#apellido').value = json.estatus_descripcion.apellido;
                document.querySelector('#cedula').value = json.estatus_descripcion.cedula;
                document.querySelector('#direccion').value = json.estatus_descripcion.direccion;
                document.querySelector('#medidor').value = json.estatus_descripcion.medidor;
                var balance = parseFloat(0);
                var listaConsumo = document.querySelector('#consumos');
                json.estatus_descripcion.consumos.forEach((item) => {
                    var registro = document.createElement('div');
                    registro.classList.add('registro');
                    registro.appendChild(agregarElemento(item.fecha));
                    registro.appendChild(agregarElemento(item.numero));
                    registro.appendChild(agregarElemento(item.monto.toLocaleString()));
                    registro.appendChild(agregarElemento('<i class="fas fa-eye" data-toggle="tooltip" title="ver" data-tipo="ver" data-id="'+item.id+'"></li>'));
                    listaConsumo.appendChild(registro);
                    balance = balance - parseFloat(item.monto);
                });
                var listaPago = document.querySelector('#pagos');
                json.estatus_descripcion.pagos.forEach((item) => {
                    var registro = document.createElement('div');
                    registro.classList.add('registro');
                    registro.appendChild(agregarElemento(item.fecha));
                    registro.appendChild(agregarElemento(item.numero));
                    registro.appendChild(agregarElemento(item.monto.toLocaleString()));
                    registro.appendChild(agregarElemento('<i class="fas fa-eye" data-toggle="tooltip" title="Ver" data-tipo="ver" data-id="'+item.id+'"></li>'));
                    listaPago.appendChild(registro);
                    balance = balance + parseFloat(item.monto);
                });
                balance = balance.toLocaleString();
                let balanceElement = document.querySelector('#balance');
                balanceElement.innerHTML = balance;
            }else{
                console.log('y');
            }
        }).catch(function(error){
            console.log(error);
        });

});

agregarElemento = (valor) => {
    var elemento = document.createElement('div');
    elemento.innerHTML = valor;
    return elemento;
}

let listaElementConsumo = document.querySelector('#consumos');
listaElementConsumo.addEventListener('click', (e) => {
    let tipo = e.target.dataset.tipo;
    let id = e.target.dataset.id;
    if (tipo && id){
        if (tipo == 'ver'){
            var actionURL = listarCunsumoURL + '/' + id;
        }
        console.log(actionURL);
        window.location.replace(actionURL);
    }
}, false);

let listaElementPago = document.querySelector('#pagos');
listaElementPago.addEventListener('click', (e) => {
    let tipo = e.target.dataset.tipo;
    let id = e.target.dataset.id;
    if (tipo && id){
        if (tipo == 'ver'){
            var actionURL = listarPagoURL + '/' + id;
        }
        console.log(actionURL);
        window.location.replace(actionURL);
    }
}, false);

