window.addEventListener('load', () => {
    let token = getCookie('tokenAcme');
    let requestURL = 'http://localhost:8000/api/1.0/cliente/' + cedula;
    fetch(
        requestURL,
        {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization' : 'Token ' + token
            }
        }).then(function(response){
            return response.json()
        }).then(function(json){
            if (json.estatus == 1){
                document.querySelector('#nombre').value = json.estatus_descripcion.nombre;
                document.querySelector('#apellido').value = json.estatus_descripcion.apellido;
                document.querySelector('#cedula').value = json.estatus_descripcion.cedula;
                document.querySelector('#direccion').value = json.estatus_descripcion.direccion;
                document.querySelector('#medidor').value = json.estatus_descripcion.medidor;
            }
        }).catch(function(error){
            console.log(error);
        });

});

agregarElemento = (valor) => {
    var elemento = document.createElement('div');
    elemento.innerHTML = valor;
    return elemento;
}

let guardar = document.querySelector('#guardar');
guardar.addEventListener('click', (e) => {
    e.preventDefault();
    let nombre = document.querySelector('#nombre');
    let apellido = document.querySelector('#apellido');
    let cedulaCliente = document.querySelector('#cedula');
    let direccion = document.querySelector('#direccion');
    let medidor = document.querySelector('#medidor');
    
    let token = getCookie('tokenAcme');
    let requestURL = 'http://localhost:8000/api/1.0/cliente/' + cedula;
    fetch(
        requestURL,
        {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization' : 'Token ' + token
            },
            body: JSON.stringify({
                'nombre': nombre.value,
                'apellido': apellido.value,
                'cedula': cedulaCliente.value,
                'direccion': direccion.value,
                'medidor': medidor.value
            })
        }).then(function(response){
            return response.json()
        }).then(function(json){
            if (json.estatus == 1){
                showAlert(json.estatus_descripcion, '', 'success', listarClienteURL);
            }else if(json.estatus == 0){
                showAlert('¡Error!', json.estatus_descripcion);
            }else{
                texto = getErrors(json)
                showAlert('¡Error!', texto);
            }
        }).catch(function(error){
            console.log(error);
        });
});

