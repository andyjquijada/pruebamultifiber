let campos = {
  username: 'Usuario',
  password: 'Contraseña',
  nombre: 'Nombre',
  apellido: 'Apellido',
  cedula: 'Cédula',
  direccion: 'Dirección',
  medidor: 'Medidor',
  numero: 'Número',
  fecha: 'Fecha',
  cantidad: 'Cantidad m<sup>3</sup>',
  cliente: 'Cliente',
  monto: 'Monto'
}

window.addEventListener('load', () => {
    if (window.location.href.indexOf("login") < 0) {
        checkCookie();
    }else{
      var username = getCookie("userAcme");
      if (username != "") {
        relocate(listarClienteURL);
      }
    }
})

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
}

function checkCookie() {
    var username = getCookie("userAcme");
    if (username == "") {
      window.location.replace(loginURL);
    }
}

function deleteCookie(cname){
    var date = Date(2000, 1, 1, 0, 0, 0);
    var expires = "expires="+ date;
    document.cookie = cname + "=;" + expires + ";path=/";
}

function showAlert(titulo, texto, icono='error', url=false){
  Swal.fire({
    icon: icono,
    title: titulo,
    html: texto,
    allowOutsideClick: false,
    allowEscapeKey: false
  }).then(function(result){
    if (result.value || url){
      relocate(url);
    }
  });
}

var confirmado = {estatus: false};

function showConfirm(titulo, texto){
  Swal.fire({
    icon: 'warning',
    title: titulo,
    html: texto,
    allowOutsideClick: false,
    allowEscapeKey: false,
    showCancelButton: true,
    confirmButtonText: 'Aceptar',
    cancelButtonText: 'Cancelar',
  }).then(function(result){
    if (result.value){
      confirmado.estatus = true
    }else if(result.dismiss === Swal.DismissReason.cancel){
      confirmado.estatus = false
    }
  });
}

function relocate(url=false){
  if (url){
    window.location.replace(url);
  }
}

function getErrors(obj){
  var errores = Object.entries(obj)
  var texto = '';
  errores.forEach(function (value, key){
    if (key > 0){
      texto = texto + '<br/>'
    }
    texto = texto + '<strong>'+campos[value[0]]+'</strong>: '+value[1][0]
  });
  return texto;
}

let logout = document.querySelector('#logout');
if (logout){
    logout.addEventListener('click', (e) => {
        e.preventDefault();
        token = getCookie('tokenAcme')
        fetch(
          'http://localhost:8000/api_borrar_token/',
          {
            method: 'delete',
            headers: {
              'Content-type': 'application/json',
              'Authorization' : 'Token ' + token
            },
            body: JSON.stringify({
              'token': token
            })
          }
        ).then(function(response){
          return response.json();
        }).then(function(json){
          console.log(json.estatus_descripcion);
        });
        deleteCookie('userAcme');
        deleteCookie('tokenAcme');
        relocate(loginURL);
    });
}
