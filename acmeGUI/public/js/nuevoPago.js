window.addEventListener('load', () => {
    token = getCookie('tokenAcme');
    fetch(
        'http://localhost:8000/api/1.0/clientes/',
        {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization' : 'Token ' + token
            }
        }
    ).then((response) => response.json()
    ).then((json) => {
        if (json.estatus == 1){
            let clientes = document.querySelector('#cliente');
            json.estatus_descripcion.forEach((item) => {
                clientes.appendChild(agregarElemento(item.id, item.nombre + ' ' + item.apellido));
            });
        }
    })
});

document.querySelector('#monto').addEventListener('blur', function(){
    if (this.value == '' || isNaN(this.value)){
        this.value = 0;
        showAlert('¡Advertencia!', 'El campo <strong>monto</strong> debe ser númerico.', 'warning')
    }
});

let registrar = document.querySelector('#registrar');
let cancelar = document.querySelector('#cancelar');
registrar.addEventListener('click', (e) => {
    e.preventDefault();
    let numero = document.querySelector('#numero');
    let fecha = document.querySelector('#fecha');
    let monto = document.querySelector('#monto');
    let cliente = document.querySelector('#cliente');    
    token = getCookie('tokenAcme');
    fetch(
        'http://localhost:8000/api/1.0/pago/',
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization' : 'Token ' + token
            },
            body: JSON.stringify({
                'numero': numero.value,
                'fecha': fecha.value,
                'monto': parseFloat(monto.value),
                'cliente': parseInt(cliente.value)
            })
        }).then(function(response){
            return response.json()
        }).then(function(json){
            if (json.estatus == 1){
                showAlert(json.estatus_descripcion, '', 'success', listarPagoURL);
            }else if(json.estatus == 0){
                showAlert('¡Error!', json.estatus_descripcion);
            }else{
                texto = getErrors(json)
                showAlert('¡Error!', texto);
            }
        }).catch(function(error){
            console.log(error);
        });
});

agregarElemento = (valor, texto) => {
    var elemento = document.createElement('option');
    elemento.value = valor;
    elemento.innerHTML = texto;
    return elemento;
}
