window.addEventListener('load', () => {
    token = getCookie('tokenAcme');
    fetch(
        'http://localhost:8000/api/1.0/consumo/',
        {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization' : 'Token ' + token
            }
        }).then(function(response){
            return response.json()
        }).then(function(json){
            if (json.estatus == 1){
                if (json.estatus_descripcion.length){
                    var lista = document.querySelector('#lista');
                    json.estatus_descripcion.forEach((item) => {
                        var registro = document.createElement('div');
                        registro.classList.add('registro');
                        registro.appendChild(agregarElemento(item.fecha));
                        registro.appendChild(agregarElemento(item.numero));
                        registro.appendChild(agregarElemento(item.consumido));
                        registro.appendChild(agregarElemento(item.monto.toLocaleString()));
                        registro.appendChild(agregarElemento('<i class="fas fa-eye" data-toggle="tooltip" title="Ver" data-tipo="ver" data-id="'+item.id+'"></i>'));
                        lista.appendChild(registro);
                    });
                }else{
                    var lista = document.querySelector('#lista');
                    var registro = document.createElement('div');
                    registro.classList.add('registro');
                    registro.classList.add('lista-vacia');
                    var elemento = document.createElement('div');
                    elemento.innerHTML = 'No existen consumos para listar';
                    registro.appendChild(elemento);
                    lista.appendChild(registro);
                }
            }
        }).catch(function(error){
            console.log(error);
        });

});

agregarElemento = (valor) => {
    var elemento = document.createElement('div');
    elemento.innerHTML = valor;
    return elemento;
}

let listaElement = document.querySelector('#lista');
listaElement.addEventListener('click', (e) => {
    let tipo = e.target.dataset.tipo;
    let id = e.target.dataset.id;
    if (tipo && id){
        if (tipo == 'ver'){
            var actionURL = listarCunsumoURL + '/' + id;
        }
        console.log(actionURL);
        window.location.replace(actionURL);
    }
}, false);
