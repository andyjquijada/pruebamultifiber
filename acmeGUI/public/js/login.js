let username = document.querySelector('#username');
    let password = document.querySelector('#password');
    let enviar = document.querySelector('#enviar');

    enviar.addEventListener('click', function(e){
        e.preventDefault();
        
        fetch(
            'http://localhost:8000/api_generar_token/',
            {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    username: username.value,
                    password: password.value
                })
            }).then(function(response){
                return response.json()
            }).then(function(json){
                if (json.token){
                    setCookie('tokenAcme', json.token, 1);
                    setCookie('userAcme', username.value, 1);
                    relocate(listarClienteURL)
                }else if (json.non_field_errors){
                    showAlert('¡Error!', json.non_field_errors[0]);
                }else{
                    texto = getErrors(json)
                    showAlert('¡Error!', texto);
                }
            }).catch(function(error){
                console.log(error);
            });
        });
