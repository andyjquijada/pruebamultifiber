<nav>
    <a class="logotipo" href="#">ACME</a>
    <div class="enlaces">
        @if(\Request::is('login'))
            <a href="#">Login</a>
        @else
            <a href="{{ route('listarCliente') }}">Clientes</a>
            <a href="{{ route('listarConsumo') }}">Consumos</a>
            <a href="{{ route('listarPago') }}">Pagos</a>
            <a href="#" id="logout">Logout</a>
        @endif
    </div>
</nav>