<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('/css/estilos.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/sweetalert2.all.min.css') }}">
    <title>Document</title>
</head>
<body>
    <div class="contenedor">
        @component('componentes.navbar')
        @endcomponent
        
        <form id="login">
            <h2>Login</h2>
            <div class="campo">
                <label for="usermane">Usuario:</label>
                <input type="text" name="username" id="username">
            </div>
            <div class="campo">
                <label for="password">Contraseña:</label>
                <input type="password" name="password" id="password">
            </div>
            <div class="botonera">
                <button type="submit" id="enviar" class="boton">Enviar</button>
            </div>
        </form>
            
        </div>
    </div>
    <script>
        let loginURL = '{{ route("loginAcme") }}';
        let listarClienteURL = '{{ route("listarCliente") }}';
    </script>
    <script src="{{ asset('js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('js/acme.js') }}"></script>
    <script src="{{ asset('js/login.js') }}"></script>
</body>
</html>
