<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('/css/estilos.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/sweetalert2.all.min.css') }}">
    <title>Acme</title>
</head>
<body>
    <div class="contenedor">
        @component('componentes.navbar')
        @endcomponent

        <form class="formulario-registro">
            <h2>Edición de cliente</h2>
            <div class="campo">
                <label for="nombre">Nombre:</label>
                <input type="text" name="nombre" id="nombre">
            </div>
            <div class="campo">
                <label for="apellido">Apellido:</label>
                <input type="text" name="apellido" id="apellido">
            </div>
            <div class="campo">
                <label for="cedula">Cédula:</label>
                <input type="text" name="cedula" id="cedula" readonly>
            </div>
            <div class="campo">
                <label for="direccion">Dirección:</label>
                <input type="text" name="direccion" id="direccion">
            </div>
            <div class="campo">
                <label for="medidor">Medidor:</label>
                <input type="text" name="medidor" id="medidor">
            </div>
            <div class="botonera">                
                <a href="#" class="boton" id="guardar">
                    Guardar
                </a>
                <a href="{{ URL::previous() }}" class="boton" id="cancelar">
                    Cancelar
                </a>
            </div>
        </form>
        
    <script>
        let loginURL = '{{ route("loginAcme") }}';
        let listarClienteURL = '{{ route("listarCliente") }}';
        let cedula = '{{ $cedula }}';
    </script>
    <script src="{{ asset('js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('js/acme.js') }}"></script> 
    <script src="{{ asset('js/editarCliente.js') }}"></script> 
</body>
</html>
