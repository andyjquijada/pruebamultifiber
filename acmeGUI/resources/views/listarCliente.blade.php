<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('/css/estilos.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/sweetalert2.all.min.css') }}">
    <title>Acme</title>
</head>
<body>
    <div class="contenedor">
        @component('componentes.navbar')
        @endcomponent

        <a href="{{ route('nuevoCliente') }}" class="boton boton-nuevo">
            Nuevo
        </a>
        
        <div class="lista" id="lista">
            <div class="registro cabecera">
                <div>Nombre</div>
                <div>Cédula</div>
                <div>Dirección</div>
                <div>Medidor</div>
                <div>Acciones</div>
            </div>
        </div>
    </div>
    <script>
        let loginURL = '{{ route("loginAcme") }}';
        let listarClienteURL = '{{ route("listarCliente") }}';
    </script>
    <script src="{{ asset('js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('js/acme.js') }}"></script>
    <script src="{{ asset('js/listarCliente.js') }}"></script>    
</body>
</html>
