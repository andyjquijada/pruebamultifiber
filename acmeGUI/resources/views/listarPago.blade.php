<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('/css/estilos.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/all.min.css') }}">
    <title>Acme</title>
</head>
<body>
    <div class="contenedor">
        @component('componentes.navbar')
        @endcomponent

        <a href="{{ route('nuevoPago') }}" class="boton boton-nuevo">
            Nuevo
        </a>
        
        <div class="lista" id="lista">
            <div class="registro cabecera pago">
                <div>Fecha</div>
                <div>Número</div>
                <div>Monto</div>
                <div>Acciones</div>
            </div>
        </div>
    </div>
    <script>
        let loginURL = '{{ route("loginAcme") }}';
        let listarClienteURL = '{{ route("listarCliente") }}';
        let listarCunsumoURL = '{{ route("listarPago") }}';
    </script>
    <script src="{{ asset('js/acme.js') }}"></script>
    <script src="{{ asset('js/listarPago.js') }}"></script>        
</body>
</html>