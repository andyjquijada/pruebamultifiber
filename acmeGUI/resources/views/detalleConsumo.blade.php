<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('/css/estilos.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/all.min.css') }}">
    <title>Acme</title>
</head>
<body>
    <div class="contenedor">
        @component('componentes.navbar')
        @endcomponent

        <form class="formulario-registro">
            <h2>Consumo</h2>
            <div class="campo">
                <label for="numero">Número:</label>
                <input type="text" name="numero" id="numero" readonly>
            </div>
            <div class="campo">
                <label for="fecha">fecha:</label>
                <input type="date" name="fecha" id="fecha" readonly>
            </div>
            <div class="campo">
                <label for="cantidad">Cantidad:</label>
                <input type="text" name="cantidad" id="cantidad" readonly>
            </div>
            <div class="campo">
                <label for="consumido">Consumido:</label>
                <input type="text" name="consumido" id="consumido" readonly>
            </div>
            <div class="campo">
                <label for="monto">Monto:</label>
                <input type="text" name="monto" id="monto" readonly>
            </div>
            <div class="campo">
                <label for="cliente">Cliente:</label>
                <input type="text" name="cliente" id="cliente" readonly>
            </div>
            <div class="botonera">                
                <a href="{{ URL::previous() }}" class="boton" id="volver">
                    Volver
                </a>
            </div>
        </form>
        
    <script>
        let loginURL = '{{ route("loginAcme") }}';
        let id = '{{ $id }}';
    </script>
    <script src="{{ asset('js/acme.js') }}"></script> 
    <script src="{{ asset('js/detalleConsumo.js') }}"></script> 
    
</body>
</html>