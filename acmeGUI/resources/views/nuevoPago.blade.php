<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('/css/estilos.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/sweetalert2.all.min.css') }}">
    <title>Acme</title>
</head>
<body>
    <div class="contenedor">
        @component('componentes.navbar')
        @endcomponent

        <form class="formulario-registro">
            <h2>Nuevo pago</h2>
            <div class="campo">
                <label for="numero">Número:</label>
                <input type="text" name="numero" id="numero">
            </div>
            <div class="campo">
                <label for="fecha">Fecha:</label>
                <input type="date" name="fecha" id="fecha">
            </div>
            <div class="campo">
                <label for="monto">Monto:</label>
                <input type="text" name="monto" id="monto" value="0">
            </div>
            <div class="campo">
                <label for="cliente">Cliente:</label>
                <select name="cliente" id="cliente">
                    <option value="">---</option>
                </select>
            </div>
            <div class="botonera"> 
                <a href="#" class="boton" id="registrar">
                    Registrar
                </a>               
                <a href="{{ URL::previous() }}" class="boton" id="cancelar">
                    Cancelar
                </a>
            </div>
        </form>
        
    <script>
        let loginURL = '{{ route("loginAcme") }}';
        let listarPagoURL = '{{ route("listarPago") }}';
    </script>
    <script src="{{ asset('js/sweetalert2.all.min.js') }}"></script>
    <script src="{{ asset('js/acme.js') }}"></script> 
    <script src="{{ asset('js/nuevoPago.js') }}"></script> 
    
</body>
</html>
