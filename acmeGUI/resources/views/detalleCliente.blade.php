<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('/css/estilos.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/all.min.css') }}">
    <title>Acme</title>
</head>
<body>
    <div class="contenedor">
        @component('componentes.navbar')
        @endcomponent

        <form class="formulario-registro">
            <h2>Cliente</h2>
            <div class="campo">
                <label for="nombre">Nombre:</label>
                <input type="text" name="nombre" id="nombre" readonly>
            </div>
            <div class="campo">
                <label for="apellido">Apellido:</label>
                <input type="text" name="apellido" id="apellido" readonly>
            </div>
            <div class="campo">
                <label for="cedula">Cédula:</label>
                <input type="text" name="cedula" id="cedula" readonly>
            </div>
            <div class="campo">
                <label for="direccion">Dirección:</label>
                <input type="text" name="direccion" id="direccion" readonly>
            </div>
            <div class="campo">
                <label for="medidor">Medidor:</label>
                <input type="text" name="medidor" id="medidor" readonly>
            </div>
            <div class="campo">
                <span>Balance</span>
                <div id="balance"></div>
            </div>
            <div class="contenedor-lista">
                <div class="lista" id="consumos">
                    <h3>Consumos</h3>
                    <div class="registro cabecera">
                        <div>Fecha</div>
                        <div>Número</div>
                        <div>Monto</div>
                        <div>Acción</div>
                    </div>
                </div>
                <div class="lista" id="pagos">
                    <h3>Pagos</h3>
                    <div class="registro cabecera">
                        <div>Fecha</div>
                        <div>Número</div>
                        <div>Monto</div>
                        <div>Acción</div>
                    </div>
                </div>
            </div>
            <div class="botonera">                
                <a href="{{ URL::previous() }}" class="boton" id="volver">
                    Volver
                </a>
            </div>
        </form>
        
    <script>
        let loginURL = '{{ route("loginAcme") }}';
        let listarCunsumoURL = '{{ route("listarConsumo") }}';
        let listarPagoURL = '{{ route("listarPago") }}';
        let cedula = '{{ $cedula }}';
    </script>
    <script src="{{ asset('js/acme.js') }}"></script> 
    <script src="{{ asset('js/detalleCliente.js') }}"></script> 
</body>
</html>