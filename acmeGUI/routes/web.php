<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/login', function(){
    return view('login');
})->name('loginAcme');

Route::get('/cliente', function(){
    return view('listarCliente');
})->name('listarCliente');

Route::get('/cliente/nuevo', function(){
    return view('nuevoCliente');
})->name('nuevoCliente');

Route::get('/cliente/{cedula}', function($cedula){
    return view('detalleCliente', ['cedula' => $cedula]);
})->name('detalleCliente');

Route::get('/cliente/editar/{cedula}', function($cedula){
    return view('editarCliente', ['cedula' => $cedula]);
})->name('editarCliente');

Route::get('/consumo', function(){
    return view('listarConsumo');
})->name('listarConsumo');

Route::get('/consumo/nuevo', function(){
    return view('nuevoConsumo');
})->name('nuevoConsumo');

Route::get('/consumo/{id}', function($id){
    return view('detalleConsumo', ['id' =>$id]);
})->name('detalleConsumo');

Route::get('/pago', function(){
    return view('listarPago');
})->name('listarPago');

Route::get('/pago/nuevo', function(){
    return view('nuevoPago');
})->name('nuevoPago');

Route::get('/pago/{id}', function($id){
    return view('detallePago', ['id' =>$id]);
})->name('detallePago');
