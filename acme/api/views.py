from django.shortcuts import render
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.views import status
from rest_framework.authtoken.models import Token
from .models import Cliente, Consumo, Pago
from .serializers import ClienteSerializer, ClienteSerializerMin, ConsumoSerializer, PagoSerializer, TokenSerializer

def get_data(dict, campo):
        return dict.get(campo, False)

class ClienteListView(generics.ListCreateAPIView):
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer

    def list(self, request, *args, **kwargs):
        cliente = self.queryset.filter(activo=True)
        data = ClienteSerializer(cliente, many=True, context={'request':request})
        return Response(
            data={
                    'estatus': 1,
                    'estatus_descripcion': data.data
                },
            status=status.HTTP_200_OK
        )

    def create(self, request, *args, **kwargs):
        cedula = get_data(request.data, 'cedula')
        cliente = self.queryset.filter(cedula=cedula)
        if len(cliente):
            return Response(
                data={
                    'estatus': 0,
                    'estatus_descripcion': 'El cliente ya se encuentra registrado'
                },
                status=status.HTTP_400_BAD_REQUEST
            )
        serializer = ClienteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            data={
                'estatus': 1,
                'estatus_descripcion': 'Registro creado satisfactoriamente'
            },
            status=status.HTTP_201_CREATED
        )

class ClienteListViewMin(generics.ListAPIView):
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializerMin

    def list(self, request, *args, **kwargs):
        cliente = self.queryset.filter(activo=True)
        data = ClienteSerializerMin(cliente, many=True)
        return Response(
            data={
                    'estatus': 1,
                    'estatus_descripcion': data.data
                },
            status=status.HTTP_200_OK
        )

    def create(self, request, *args, **kwargs):
        cedula = get_data(request.data, 'cedula')
        cliente = self.queryset.filter(cedula=cedula)
        if len(cliente):
            return Response(
                data={
                    'estatus': 0,
                    'estatus_descripcion': 'El cliente ya se encuentra registrado'
                },
                status=status.HTTP_400_BAD_REQUEST
            )
        serializer = ClienteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            data={
                'estatus': 1,
                'estatus_descripcion': 'Registro creado satisfactoriamente'
            },
            status=status.HTTP_201_CREATED
        )

class ClienteUpdateView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer

    def retrieve(self, request, *args, **kwargs):
        cedula = get_data(kwargs,'cedula')
        cliente = self.queryset.get(cedula=cedula)
        data = ClienteSerializer(cliente)
        return Response(
            data={
                    'estatus': 1,
                    'estatus_descripcion': data.data
                },
            status=status.HTTP_200_OK
        )

    def update(self, request, *args, **kwargs):
        cedula = get_data(kwargs, 'cedula')
        cliente = self.queryset.get(cedula=cedula)
        serializer = ClienteSerializer(instance=cliente, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            data={
                'estatus': 1,
                'estatus_descripcion': 'Registro actualizado con exito'
            },
            status=status.HTTP_200_OK
        )

    def destroy(self, request, *args, **kwargs):
        cedula = get_data(kwargs, 'cedula')
        cliente = self.queryset.get(cedula=cedula)
        cliente.activo = False
        cliente.save()
        return Response(
            data={
                'estatus': 1,
                'estatus_descripcion': 'Registro eliminado con exito'
            },
            status=status.HTTP_200_OK
        )

    def update(self, request, *args, **kwargs):
        cedula = get_data(kwargs, 'cedula')
        cliente = self.queryset.get(cedula=cedula)
        serializer = ClienteSerializer(instance=cliente, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            data={
                'estatus': 1,
                'estatus_descripcion': 'Registro actualizado con exito'
            },
            status=status.HTTP_200_OK
        )

    def destroy(self, request, *args, **kwargs):
        cedula = get_data(kwargs, 'cedula')
        cliente = self.queryset.get(cedula=cedula)
        cliente.activo = False
        cliente.save()
        return Response(
            data={
                'estatus': 1,
                'estatus_descripcion': 'Registro eliminado con exito'
            },
            status=status.HTTP_200_OK
        )


class ConsumoList(generics.ListCreateAPIView):
    queryset = Consumo.objects.all()
    serializer_class = ConsumoSerializer

    def list(self, request, *args, **kwargs):
        consumo = self.queryset.all()
        data = ConsumoSerializer(consumo, many=True)
        return Response(
            data={
                    'estatus': 1,
                    'estatus_descripcion': data.data
                },
            status=status.HTTP_200_OK
        )

    def create(self, request, *args, **kwargs):
        numero = get_data(request.data, 'numero')
        consumo = self.queryset.filter(numero=numero)
        if len(consumo):
            return Response(
                data={
                    'estatus': 0,
                    'estatus_descripcion': 'El número de registro ingresado ya se encuentra registrado'
                },
                status=status.HTTP_400_BAD_REQUEST
            )
        ultimo_consumo = self.queryset.filter(cliente=request.data.get('cliente')).order_by('-fecha')
        if not len(ultimo_consumo):
            request.data['consumido'] = float(request.data.get('cantidad'))
        else:
            request.data['consumido'] = float(request.data.get('cantidad')) - ultimo_consumo[0].cantidad
        if request.data['consumido'] < 1:
            request.data['monto'] = 100
        else:
            request.data['monto'] = request.data.get('consumido') * 1250
        print(request.data)
        serializer = ConsumoSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(
            data={
                'estatus': 1,
                'estatus_descripcion': 'Registro creado satisfactoriamente'
            },
            status=status.HTTP_201_CREATED
        )

class ConsumoDetail(generics.ListAPIView):
    queryset = Consumo.objects.all()
    serializer_class = ConsumoSerializer

    def list(self, request, *args, **kwargs):
        id = get_data(kwargs, 'id')
        consumo = self.queryset.get(id=id)
        data = ConsumoSerializer(consumo)
        cliente = Cliente.objects.get(id=data.data['cliente'])
        data = data.data
        data['cliente'] = cliente.nombre + ' ' + cliente.apellido
        print(data)
        return Response(
            data={
                    'estatus': 1,
                    'estatus_descripcion': data
                },
            status=status.HTTP_200_OK
        )


class PagoList(generics.ListCreateAPIView):
    queryset = Pago.objects.all()
    serializer_class = PagoSerializer

    def list(self, request, *args, **kwargs):
        pago = self.queryset.all()
        data = PagoSerializer(pago, many=True)
        return Response(
            data={
                    'estatus': 1,
                    'estatus_descripcion': data.data
                },
            status=status.HTTP_200_OK
        )

    def create(self, request, *args, **kwargs):
        print(request.data)
        numero = get_data(request.data, 'numero')
        pago = self.queryset.filter(numero=numero)
        if len(pago):
            return Response(
                data={
                    'estatus': 0,
                    'estatus_descripcion': 'El número de pago ingresado ya se encuentra registrado'
                },
                status=status.HTTP_400_BAD_REQUEST
            )
        serializer = PagoSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        
        return Response(
            data={
                'estatus': 1,
                'estatus_descripcion': 'Registro creado satisfactoriamente'
            },
            status=status.HTTP_201_CREATED
        )

class PagoDetail(generics.ListAPIView):
    queryset = Pago.objects.all()
    serializer_class = PagoSerializer

    def list(self, request, *args, **kwargs):
        id = get_data(kwargs, 'id')
        pago = self.queryset.filter(id=id)
        data = PagoSerializer(pago)
        cliente = Cliente.objects.get(id=data.data['cliente'])
        data = data.data
        data['cliente'] = cliente.nombre + ' ' + cliente.apellido
        return Response(
            data={
                    'estatus': 1,
                    'estatus_descripcion': data.data
                },
            status=status.HTTP_200_OK
        )

class TokenDestroy(generics.DestroyAPIView):
    queryset = Token.objects.all()
    serializer_class = TokenSerializer

    def destroy(self, request, *args, **kwargs):
        userToken = request.data.get('token', False)
        if not userToken:
            return Response(
                data = {
                    'estatus': 0,
                    'estatus_descripcion': 'No existe token para el usuario'
                }
            )
        token = self.queryset.get(key=userToken)
        token.delete()
        return Response(
            data = {
                'estatus': 1,
                'estatus_descripcion': 'Token eliminado con exito'
            }
        )

