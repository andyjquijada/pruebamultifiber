from django.urls import path
from .views import ClienteListView, ClienteUpdateView, ClienteListViewMin, ConsumoList, ConsumoDetail, PagoList, PagoDetail
 
urlpatterns = [
    path('cliente/', ClienteListView.as_view(), name='cliente_list'),
    path('cliente/<cedula>', ClienteUpdateView.as_view(), name='cliente_update'),
    path('clientes/', ClienteListViewMin.as_view(), name='cliente_list_min'),
    path('consumo/', ConsumoList.as_view(), name='consumo_list'),
    path('consumo/<id>', ConsumoDetail.as_view(), name='consumo_detail'),
    path('pago/', PagoList.as_view(), name='pago_list'),
    path('pago/<id>', PagoDetail.as_view(), name='pago_detail')
]