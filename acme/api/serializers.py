from rest_framework import serializers
from rest_framework.authtoken.models import Token
from .models import Cliente, Consumo, Pago

class PagoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pago
        fields = (
            'id',
            'numero',
            'fecha',
            'monto',
            'cliente',
        )

class ConsumoSerializer(serializers.ModelSerializer):    
    class Meta:
        model = Consumo
        fields = (
            'id',
            'numero',
            'fecha',
            'cantidad',
            'consumido',
            'monto',
            'cliente'
        )

class ClienteSerializer(serializers.ModelSerializer):
    consumos = ConsumoSerializer(many=True, allow_null=True, default=[], read_only=True)
    pagos = PagoSerializer(many=True, allow_null=True, default=[], read_only=True)

    class Meta:
        model = Cliente
        fields = (
            'id', 
            'nombre',
            'apellido',
            'cedula',
            'direccion',
            'medidor',
            'consumos',
            'pagos'
            )

class ClienteSerializerMin(serializers.ModelSerializer):
    class Meta:
        model = Cliente
        fields = (
            'id', 
            'nombre',
            'apellido',
            )

class TokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Token
        fields = (
            'key'
        )