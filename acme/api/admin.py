from django.contrib import admin
from .models import Cliente, Consumo, Pago

admin.site.register(Cliente)
admin.site.register(Consumo)
admin.site.register(Pago)
