from django.db import models
from django.core.validators import MinValueValidator
from .choices import ESTATUS_CONSUMO

class Cliente(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField('Nombre', max_length=50)
    apellido = models.CharField('Apellido', max_length=50)
    cedula = models.CharField('Cédula', max_length=10)
    direccion = models.CharField('Dirección', max_length=200)
    medidor = models.CharField('Medidor', max_length=8)
    activo = models.BooleanField('Activo', default=True)
    creado = models.DateTimeField(auto_now_add=True)
    modificado = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '[{}]{} {}'.format(self.medidor, self.nombre, self.apellido)
    
class Consumo(models.Model):
    id = models.AutoField(primary_key=True)
    numero = models.CharField('Número', max_length=50)
    fecha = models.DateField('Fecha')
    cantidad = models.FloatField('Cantidad', validators=[MinValueValidator(0.1)])
    consumido = models.FloatField('Consumido')
    monto = models.FloatField('Monto')
    cliente_id = models.ForeignKey(Cliente, name='cliente', on_delete=models.CASCADE, related_name='consumos')

    def __str__(self):
        return self.numero

class Pago(models.Model):
    id = models.AutoField(primary_key=True)
    numero = models.CharField('Número', max_length=50)
    fecha = models.DateField('Fecha')
    monto = models.FloatField('Monto', validators=[MinValueValidator(0.1)])
    cliente_id = models.ForeignKey(Cliente, name='cliente', on_delete=models.CASCADE, related_name='pagos')

    def __str__(self):
        return self.numero

